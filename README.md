# Pyblog

Python script written to make updating my blog easier

# Usage
Simply run pyblog and it will ask you for a post title,  and then contents line by line. It will then update a file called blog.html and rss.xml

# Assumptions
As of right now Pyblog assumes your main blog page is called blog.html and resides in the same folder as the script. It also assumes you have a template file with the divisions, and headers, etc in the same folder. 
Lastly it assumes your rss.xml file is one folder above where the blogs are. This is due to my blogs being in a folder called Blogs being separate from the main site folder. I may work to add a layer of customizability to this later to specify it in a var or something at the top of the file.
